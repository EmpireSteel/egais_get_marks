"""
Имя: Автоматизация запроса марок на УТМ
Версия: 1.0
Дата: 20.03.2019
Автор: Гаврилов Андрей
Email: andrey.gavrilov@x5.ru
Tel: 10383
"""

import requests
from xml.etree import ElementTree
import time
from datetime import datetime
import os


def create_xml(FSRAR, Bcode):
    with open(os.path.join(os.getcwd(),'QueryRestBCode.xml'), 'w') as f:
        f.write("""<?xml version="1.0" encoding="UTF-8"?>
<ns:Documents Version="1.0"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:ns="http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01"
xmlns:qp="http://fsrar.ru/WEGAIS/QueryParameters">
<ns:Owner>
<ns:FSRAR_ID>""" + FSRAR + """</ns:FSRAR_ID> 
</ns:Owner>
<ns:Document>
<ns:QueryRestBCode>
<qp:Parameters>
<qp:Parameter>
<qp:Name>""" + Bcode + """</qp:Name>
<qp:Value>""" + Bcode + """</qp:Value>
</qp:Parameter>
</qp:Parameters>
</ns:QueryRestBCode>
</ns:Document>
</ns:Documents>""")


if __name__ == "__main__":
    with open(os.getcwd() + '/work.csv', 'r') as f:
        file = f.readlines()
        header = file[0]
        lines = file[1:]
    report_name = os.getcwd() + '/report-' + str(datetime.now()).split('.')[0].replace(':','-').replace(' ','_') + '.csv'
    with open(report_name, 'w') as report:
        report.write(header.rstrip() + ';ip;марки получены\n')
    for i in ['/results', '/server']:
        if not os.path.isdir(os.getcwd() + i):
            os.mkdir(os.getcwd() +  i)
    count = 0
    stores = {}
    for line in (lines):
        store = line.split(';')[0]
        FSRARID = line.split(';')[1]
        if len(FSRARID) == 11 and not FSRARID.startswith('0'):
            FSRARID = '0' + FSRARID
        Bcode = line.split(';')[3]
        Acode = line.split(';')[2]
        ip = line.split(';')[4].rstrip()
        if store not in stores:
            stores[store] = {'ip': ip , 'timeout': 0, 'done': 0, 'Acode': Acode, 'jobs': [{'FSRARID': FSRARID, 'Bcode': Bcode}]}
        else:
            stores[store]['jobs'].append({'FSRARID': FSRARID, 'Bcode': Bcode})
    done_count = 0
    min_time = 600
    while done_count != len(lines):
        for store in stores:
            print(stores[store])
            if stores[store]['timeout'] == 0 or int(time.time()) - stores[store]['timeout'] >= 600:
                if stores[store]['done'] < len(stores[store]['jobs']):
                    print('------------------------------------------------------------------------------------------------')
                    print(done_count, 'готово из', len(lines))
                    print('Создание xml файла', end='...')
                    create_xml(stores[store]['jobs'][stores[store]['done']]['FSRARID'], stores[store]['jobs'][stores[store]['done']]['Bcode'])
                    print('Готово')
                    url = 'http://' + stores[store]['ip'] + ':8195/opt/in/QueryRestBCode'
                    files = {'xml_file': open('QueryRestBCode.xml', 'rb')}
                    print('Отправка xml файла на УТМ с адресом', url.split('opt')[0], end=' ... ')
                    send_response = requests.post(url, files=files)
                    if send_response.status_code == 200:
                        print('Готово')
                        send_time = int(datetime.timestamp(datetime.utcnow()))
                        wait = True
                        while wait:
                            if int(datetime.timestamp(datetime.utcnow())) - send_time < 600:
                                print('Ожидание ответа от сервера (обновление каждые 30 секунд)', end='\r' )
                                wait_response = requests.get('http://' + stores[store]['ip'] + ':8195/opt/out/ReplyRestBCode')
                                root = ElementTree.fromstring(wait_response.content)
                                elems = [elem for elem in root.iter() if elem.tag == 'url']
                                if elems:
                                    file_time = elems[-1].attrib['timestamp'][:19]
                                    time_zone = int(elems[-1].attrib['timestamp'][23:26])
                                    file_time = file_time[:11] + str(int(file_time[11:13]) - time_zone) + file_time[13:]
                                    file_time = datetime.timestamp(datetime.strptime(file_time, "%Y-%m-%dT%H:%M:%S"))
                                    if file_time > send_time:
                                        file_url = elems[-1].text
                                        file_id = elems[-1].attrib['fileId']
                                        wait = False
                                    else:
                                        time.sleep(30)
                            else:
                                print('')
                                send_response = requests.post(url, files=files)
                                print("Ответ не получен в течении 10 минут. Повторная отправка запроса", end=' ... ')
                                send_time = int(datetime.timestamp(datetime.utcnow()))
                                if send_response.status_code == 200:
                                    print('Готово')
                                else:
                                    print('Ошибка')
                                    continue
                        print('Ответ от сервера получен' )
                        print('Получение файла из ответа', end='...' )
                        get_response = requests.get(file_url)
                        print('Готово')
                        if not os.path.isdir(os.path.join(os.getcwd(), 'server', store)):
                            os.mkdir(os.path.join(os.getcwd(), 'server', store))
                        print('Запись xml в server/' + store + '/ReplyRestBCode-' + file_id + '.xml', end='...')
                        with open(os.getcwd() + '/server/' + store + '/ReplyRestBCode-' + file_id + '.xml', 'w') as f:
                            f.write(get_response.text)
                        print("Готово")
                        FSRAR_ID = ''
                        Inform2RegId = ''
                        MarkInfo = []
                        file = os.getcwd() + '/server/' + store + '/ReplyRestBCode-' + file_id + '.xml'
                        print("Чтение файла:", file)
                        with open(file) as f:
                            for line in f.readlines():
                                if 'FSRAR_ID' in line:
                                    FSRAR_ID = line.split('>')[1].split('<')[0]
                                    print('FSRAR_ID найден:', FSRAR_ID)
                                elif 'Inform2RegId' in line:
                                    Inform2RegId = line.split('>')[1].split('<')[0]
                                    print('Inform2RegId найден:', Inform2RegId)
                                elif line.startswith('<ce:amc>'):
                                    MarkInfo.append(line.split('>')[1].split('<')[0])
                                    print('MarkInfo найден:', line.split('>')[1].split('<')[0])
                        if not os.path.exists(os.getcwd() + '/results/' + store):
                            os.mkdir(os.getcwd() + '/results/' + store)
                        if not os.path.exists(os.getcwd() + '/results/' + store + '/result.csv'):
                            with open(os.getcwd() + '/results/' + store + '/result.csv', 'w') as f:
                                f.write(';'.join(['FSRAR_ID', 'Справка Б', 'Марки']) + '\n') 
                        with open(os.getcwd() + '/results/' + store + '/result.csv', 'a') as f:
                            for mark in MarkInfo:
                                f.write(';'.join([FSRAR_ID, Inform2RegId, mark]) + '\n')
                        done_count += 1
                        with open(report_name, 'a') as report:
                            report.write(';'.join([store, stores[store]['jobs'][stores[store]['done']]['FSRARID'], stores[store]['Acode'],stores[store]['jobs'][stores[store]['done']]['Bcode'], stores[store]['ip']]) + ';ДА\n')
                        stores[store]['done'] += 1
                        stores[store]['timeout'] = int(datetime.timestamp(datetime.utcnow()))
                    else:
                        with open(report_name, 'a') as report:
                            report.write(line.rstrip() + ';' + 'Ошибка отправки файла' + '\n')
            else:            
                if (stores[store]['done'] < len(stores[store]['jobs']) and min_time > 600 - (int(datetime.timestamp(datetime.utcnow())) - stores[store]['timeout'])) or min_time <= 1:
                    min_time = 600 - (int(datetime.timestamp(datetime.utcnow())) - stores[store]['timeout'])
    os.remove(os.getcwd() + '/QueryRestBCode.xml')
    input('Все документы отправлены. Коды ответов находятся в отчете.')